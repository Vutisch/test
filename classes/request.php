<?php

/**
 * Class AjaxRequest
 * @package AjaxRequest
 */
class AjaxRequest
{
    public $data;
    public $code;
    public $message;
    public $status;
    public $actions = array(
        "login" => "login",
        "register" => "register",
    );

    /**
     * AjaxRequest constructor.
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
        $this->action = $this->getRequestParam("act");

        if (!empty($this->actions[$this->action])) {
            $this->act = $this->actions[$this->action];
        } else {
            $this->setFieldError("400", "HTTP/ 400 Bad Request");
        }

        $this->response = $this->renderToString();
    }

    /**
     * @param $name
     * @return bool|string
     */
    public function getRequestParam($name)
    {
        if (array_key_exists($name, $this->request)) {
            return strip_tags(htmlspecialchars($this->request[$name]));
        }
        return false;
    }

    /**
     * @param $key
     * @param $value
     */
    public function setResponse($key, $value)
    {
        $this->data = $value;
    }

    /**
     * @param $name
     * @param string $message
     */
    public function setFieldError($name, $message = "")
    {
        $this->status = "error";
        $this->code = $name;
        $this->message = $message;

    }

    /**
     * @return false|string
     */
    public function renderToString()
    {
        $this->json = array(
            "status" => $this->status,
            "code" => $this->code,
            "message" => $this->message,
            "data" => $this->data,
        );

        return json_encode($this->json, ENT_NOQUOTES);
    }

    /**
     * create json
     */
    public function showResponse()
    {
        header("Content-Type: application/json; charset=UTF-8");
        echo $this->response;
    }
}
