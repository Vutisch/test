<?php

include 'ajax.php';

$ajaxRequest = new Ajax($_REQUEST);

switch ($ajaxRequest->act) {
    case 'login':
        $ajaxRequest->login();
        break;
    case 'register':
        $ajaxRequest->register();
        break;
}

$ajaxRequest->showResponse();



