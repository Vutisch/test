<?php

include "./classes/user.php";
include "./classes/request.php";
session_start();

/**
 * Class Ajax
 */
class Ajax extends AjaxRequest
{
    /**
     * authentication users
     */
    public function login()
    {
         if(!$this->requestMethod()){
             return;
         };

        $username =  $this->getRequestParam ('username');
        $password =  $this->getRequestParam ('password');
        $user = new User($username, $password);
        $auth_result = $user->authorize( );

        if (!$auth_result) {
            $this->setFieldError("password", "incorrect username or password");
        } else {
            $this->status = "ok";
            $this->setResponse("redirect", $username);
            $this->message = sprintf("Hello, %s! Access granted.", $username);
            $user->saveSession();
        }

        $this->response = $this->renderToString();
    }

    /**
     * registration users
     */
    public function register()
    {
        $this->requestMethod();
        $login = $this->getRequestParam("login");
        $name = $this->getRequestParam("username");
        $email = $this->getRequestParam("registration_email");
        $password_first = $this->getRequestParam("registration_password_first");
        $password_second = $this->getRequestParam("registration_password_second");

        if ($password_first !== $password_second) {
            $this->setFieldError("password_second", "Пароли не совпадают");
            $this->response = $this->renderToString();

            return ;
        }

        $user = new User($login, $password_first);
        $new_user = $user->create( $email,$name);

        if($new_user  === false ) {
            $this->message = sprintf("Hello, %s! Thank you for registration.", $login);
            $this->setResponse("redirect", $login);
            $this->status = "ok";
            $user->saveSession();
        } else {
            $this->setFieldError( "existing_".$new_user, 'Такой ' .$new_user .' уже существуют');
        }

        $this->response = $this->renderToString();

    }

    /**
     * @return bool
     */
    public function requestMethod()
    {
        if ($_SERVER["REQUEST_METHOD"] !== "POST") {
            $this->setFieldError("main", "Error 405 Method Not Allowed");
            $this->response = $this->renderToString();

            return false;
        }

        return true;
    }
}