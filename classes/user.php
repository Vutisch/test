<?php


/**
 * Class User
 * @package User
 */
class User
{
    const URL = 'db.xml';
    private $user_id;
    private $username;
    private $password;
    private $hash;
    private $email;
    private $is_authorized = false;

    /**
     * User constructor.
     * @param null $username
     * @param null $password
     */
    public function __construct($username = null, $password = null) {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return bool
     */
    public static function isAuthorized()
    {
        if (!empty($_SESSION["user_id"])) {
            return (bool) $_SESSION["user_id"];
        }

        return false;
    }

    /**
     * @param $password
     * @return bool|string
     */
    public function hashPassword($password)
    {
        return password_hash( $password, PASSWORD_BCRYPT);
    }

    /**
     * @param $password
     * @param $hash
     * @return bool
     */
    public function verifyPassword($password,$hash)
    {
        if (password_verify($password, $hash)) {
            return true;
        }

        return false;
    }

    /**
     * @param $username
     * @return bool
     */
    public function getUser($username)
    {
        if ($this->parseXml($username)->login == $username) {
            if($this->verifyPassword( $this->password,$this->parseXml($username)->hash)) {
                return true;
            };
        }

        return false;
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        $user = $this->getUser($this->username);

        if (!$user) {
            $this->is_authorized = false;
        } else {
            $this->is_authorized = true;
        }

        return $this->is_authorized;
    }

    /**
     * inicial Session
     */
    public function saveSession()
    {
        $_SESSION["user_id"] = $this->username;
    }

    /**
     * @param $unic
     * @return \SimpleXMLElement
     */
    public function parseXml($unic)
    {
        foreach ($this->connectDB()->user as $item) {
            if ($item->login== $unic) {
                $user = $item;
            } elseif ($item->email== $unic) {
                $user = $item;
            }
        }

        return  $user;
    }

    /**
     * @return \SimpleXMLElement
     */
    public function connectDB()
    {
        return $xml = simplexml_load_file(self::URL);
    }

    /**
     * @param $email
     * @param $name
     * @return bool|string
     */
    public function create($email,$name)
    {
        if ($this->parseXml($this->username)->login == $this->username) {
            return 'login';
        }

        if ($this->parseXml($email)->email == $email) {
            $this->parseXml($email);

            return 'email';
        }

        $xml =$this->connectDB();
        $last_track = count($xml ) - 1;
        $content= $xml ->addChild('user');
        $user_id = $xml ->user[$last_track]->user_id +1;
        $content->addChild('id',$user_id );
        $content->addChild('login',$this->username);
        $content->addChild('name', $name);
        $content->addChild('hash', $this->hashPassword( $this->password));
        $content->addChild('email', $email);
        $xml->asXML(self::URL);

        return false;
    }
}