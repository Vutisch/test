$( document ).ready(function() {
    $("form" ).submit(function() {
        event.preventDefault();
        $.ajax({
            type:$(this).attr('method'),
            cache: false,
            url: "main.php",
            data:  $(this).serialize(),
            success: function(data) {
                var text = '';
                for (var n in data)
                    if (data['status'] =='ok') {
                        text = "Здравствуйте " + data['data'];
                        window.location.href = "test.php?login="+data['data'];
                    } else if (data['status'] =='error') {
                        switch(data['code']) {
                            case "password":
                            case "main":
                            case "400":
                                text = "Error! " + data['message'];
                                break;
                            case "password_second":
                            case "existing_login":
                            case "existing_email":
                                text = "Ошибка при регистрации: " + data['message'];
                                break;
                              default:
                                //default
                            }
                        }
                alert(text);
            }
        }).done(function () {

        });

        return true;
    });
});


