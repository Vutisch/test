<?php
/**
 * Created by PhpStorm.
 * User: vutisch
 * Date: 13.10.18
 * Time: 9:11
 */

class Route {


    function render($view, $vars= array())
    {
        require_once 'vendor/autoload.php';
        $loader = new Twig_Loader_Filesystem('/');

        $twig = new Twig_Environment($loader);

        $template = $twig->loadTemplate('Resources/'.$view.'.html.twig');

        echo $template->render(array('login' => $vars));

    }

}


